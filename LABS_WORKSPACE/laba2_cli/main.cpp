#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <termios.h>


using namespace std;

bool sendSome(int to, char* data, size_t size)
{
    int bytes_send = 0;
    while(bytes_send != size)
    {
        bytes_send = send(to, data, size, 0);
        if (bytes_send == -1)
        {
            printf("Error %d:%s\n" ,errno,strerror(errno));

        }
        cout << "Sending: " << bytes_send << " bytes" << endl;
    }
    return true;
}

bool receiveSome(int from, /*const*/ char* data, size_t size)
{
    int received = 0;
    int bytes_received = 0;

    cout << "\nReceiving started!" << endl;
    cout << "Received: " << bytes_received << endl;
    cout << "Bytes to receive: " << size << endl << endl;

    while (bytes_received != size)
    {
        received = recv(from, /*(char*)*/data, size-bytes_received, 0);
        if (received == -1)
        {
            printf("Error %d:%s\n" ,errno,strerror(errno));
        }
        bytes_received += received;
        cout << "Received total: " << bytes_received << endl;
        cout << "Left: " << size - bytes_received << endl;
    }
    return true;
}

int main()
{
    cout << "Client!" << endl;


    int channel = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    sockaddr_in server;

    server.sin_family = AF_INET; 

    cout << "Enter Server IP: ";
    string address;
    cin >> address;
    server.sin_addr.s_addr =  inet_addr(address.c_str());
//    server.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");

    cout << "Enter Port: ";
    unsigned short port;
    cin >> port;
    server.sin_port = htons(port);
//    server.sin_port = htons(1234);

    int result = connect(channel, (const sockaddr*)(&server), sizeof(server));

    if (result != 0)
    {
        cout << "Connection failed!\n";
        return -1;
    }

    bool connected = true;
    int key = 0;
    string str;
    while (connected)
    {
        key = 0;
        cout.clear();
        cin.sync();
        cout << "\n*****MENU*****\n"
             << "1 - Input message\n"
             << "2 - Receive feedback\n"
             << "3 - Shutdown\n";
        
		cin >> key;
        cout << endl;


        switch(key)
        {
        case 1:
        {
            cout.clear();
            cin.sync();

            cout << "Input filename or /quit: ";
            cin>>str;
			//getline(cin, str);

            char msgType = '\0';
            if (strcmp(str.data(), "/q") == 0)
            {
                msgType = 'Q';
            }
            else
            {
                msgType = 'R';
            }

            vector<char> fileName(str.begin(), str.end());

            //size_t sizeforsending = data.size();
            uint64_t sizeForSending = fileName.size();
            cout << "Str size is: " << sizeForSending << endl;

            if (sendSome(channel, (char*)&sizeForSending, sizeof(sizeForSending)))
            {
                if(sendSome(channel, &msgType, sizeof(msgType)))
                {
                    if (msgType == 'Q')
                    {
                        close(channel);
                        return 0;
                    }
                    if(sendSome(channel, fileName.data(), sizeForSending))
                    {
                        cout << "sended\n";
                    }
                    else
                    {
                        cout << "wrong sending msg\n";
                    }
                }
                else
                {
                    cout << "wrong sending type\n";
                }
            }
            else
            {
                cout << "wrong sending size\n";
            }
            break;
        }

        case 2:
        {
            uint64_t msgSize = 0;

            if (receiveSome(channel, (char*)&msgSize, sizeof(msgSize)))
            {
                cout << "Msg size is: " << msgSize << endl;
                char msgType = '\0';

                if (receiveSome(channel, &msgType, sizeof(msgType)))
                {
                    cout << "Msg Type is: " <<  msgType << endl;
                    vector<char> msg;
                    msg.resize(msgSize+1, '\0');

                    if (receiveSome(channel, msg.data(), msgSize))
                    {
                        if (msgType != 'E')
                        {
                            char key = '\0';
                            cout << "<S>ave or <p>rint file: ";
                            cin >> key;
                            if ((key == 's') || (key == 'S'))
                            {
                                cout << "name: " << str << endl;
                                ofstream output(str.c_str(), ofstream::out);
                                output.write(msg.data(), msg.size());
                                output.close();
                            }
                            else if ((key == 'p') || (key == 'P'))
                            {
                                cout << msg.data() << endl;
                                break;
                            }
                        }
                        else
                        {
                            cout << msg.data() << endl;
                            break;
                        }
                    }
                }
            }
            break;
        }

        case 3:
            {
                close(channel);
                return 0;
            }
        default:
            cout << "Wrong key!\n";
            break;
        }
    }
    return 0;
}
